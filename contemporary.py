from PIL import Image
import numpy as np
from scipy.ndimage import convolve1d

filename = "img.png"

rshift = 8
width, height = 600, 450  # size of final image
arr = np.ones((height, width, 3)) * 128  # set all to mid-grey

# set "seed" colors
# arr[1:, 0] = [128, 128, 32]
arr[1:, 0] = [32, 64, 128]
# add a stripe near the bottom
arr[: height // 2 + 100, 0] = [32, np.random.randint(32, 128), 64]
# mix the colors along the initial axis
arr[1:, 0] = arr[:-1, 0] + np.random.randint(-rshift, rshift + 1, size=(height - 1, 3))

# shift colors around...?
for x in range(1, width):
    # convolve along axis=0 (row)
    arr[1:-1, x] = convolve1d(arr[:, x - 1], np.ones(3), axis=0, mode="constant")[
        1:-1
    ] / 3 + np.random.randint(-rshift, rshift + 1, size=(height - 2, 3))
# arr = arr.clip(0, 255)  # limit values between 0 and 255

# mix it back the other way
for x in range(width - 2, 0, -1):
    # convolve along axis=0 (row)
    arr[1:-1, x] = convolve1d(arr[:, x + 1], np.ones(3), axis=0, mode="constant")[
        1:-1
    ] / 3 + np.random.randint(-rshift, rshift + 1, size=(height - 2, 3))
arr = arr.clip(0, 255)  # limit values between 0 and 255

# save image
im = Image.fromarray(arr.astype(np.uint8)).convert("RGBA")
im.save(filename)
