# digital-art

Exploring a few ways to make digital art in Python.

## kinetic

Example: https://twitter.com/SeanSLeBlanc/status/982648532296503304

PICO8 has a display of 128 x 128

```pico
for i=1,16 do
    # replace the sixteen colors with a sequence of colors defined below
    # extract the substring for the number
    pal(i - 1, sub("029878920", i / 2, i / 2), 1)  # https://pico-8.fandom.com/wiki/Pal, https://pico-8.fandom.com/wiki/Sub
end

# clear the graphics buffer
cls()  # https://pico-8.fandom.com/wiki/Cls

# jump point
::q::
x = rnd(128)  # random float up to 128
y = rnd(128)
# https://pico-8.fandom.com/wiki/Time
# time since start, incremental counter, float
a = 64 + 32 * sin(t() / 2) - x
b = 64 + 32 * cos(t() / 3) - y
z = atan2(a, b) + t() / 4

# https://pico-8.fandom.com/wiki/Line
# draw a line between two points
line(
    x,
    y,
    x + cos(z) * 4,
    y + sin(z) * 4,
    (pget(x, y) + pget(x + 1, y) + pget(x, y + 1) + pget(x + 1, y + 1)) / 4 + 1  # color
    # https://pico-8.fandom.com/wiki/Pget
    # read the color value at the given point
)

# jump back up and repeat
goto q
```

## contemporary

Example: https://scipython.com/blog/computer-generated-contemporary-art-update/

```python
import sys
import numpy as np
from scipy.ndimage import convolve1d
from PIL import Image

try:
    filename = sys.argv[1]
except IndexError:
    filename = 'img.png'

rshift = 8
width, height = 600, 450
arr = np.ones((height, width, 3)) * 128

arr[1:,0] = [128, 128, 32]
arr[:height//2+100, 0] = [64, np.random.randint(64, 192), 128]
arr[1:,0] = arr[:-1,0] + np.random.randint(-rshift, rshift+1, size=(height-1, 3))


for x in range(1, width):
    arr[1:-1,x] = (convolve1d(arr[:,x-1], np.ones(3), axis=0,
                              mode='constant')[1:-1]/3 +
                   np.random.randint(-rshift, rshift+1, size=(height-2, 3)))
arr = arr.clip(0, 255)

im = Image.fromarray(arr.astype(np.uint8)).convert('RGBA')
im.save(filename)
```
