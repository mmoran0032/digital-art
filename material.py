import rich

logo = r"""[bold blue]
   / / / /\
  / / / /[bold black]\ [bold blue]\
 / / / /[bold black]\ \ [bold blue]\
/ / / /[bold black]\ \ \ [bold blue]\
\ \ \ \\[bold black]/ / / [bold blue]/
 \ \ \ \\[bold black]/ / [bold blue]/
  \ \ \ \\[bold black]/ [bold blue]/
   \ \ \ \/
"""
rich.print(logo)
