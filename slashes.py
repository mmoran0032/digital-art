import random
import shutil
from typing import Tuple


def generate_output(**kwargs) -> None:
    create_output(*shutil.get_terminal_size(), **kwargs)


def create_output(
    width: int,
    height: int,
    charset: Tuple[str] = ("/", "\\"),
    weights: Tuple[float] = (0.5, 0.5),
) -> str:
    """create the full terminal output"""
    lines = [create_line(width, charset, weights) for _ in range(height)]
    return "\n".join(lines)


def create_line(width: int, charset: Tuple[str], weights: Tuple[float]) -> str:
    """create a single line of the artwork with provided characters"""
    output_chars = random.choices(population=charset, weights=weights, k=width)
    return "".join(output_chars)


if __name__ == "__main__":
    print(
        generate_output(),
        generate_output(charset=("/", " ", "\\"), weights=(0.2, 0.6, 0.2)),
        sep="\n\n",
    )
